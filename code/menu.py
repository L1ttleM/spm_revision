import os, sys
import env
import random

from code.dataloader import DataLoader
from code.game import Game, GameType


class Menu:
    @staticmethod
    def clear():
        # for windows
        if os.name == 'nt':
            _ = os.system('cls')
        # for mac and linux(here, os.name is 'posix')
        else:
            _ = os.system('clear')

    @staticmethod
    # Execute menu
    def exec_menu(choice):
        Menu.clear()
        ch = choice.lower()
        if ch == '':
            menu_actions['main_menu']()
        else:
            try:
                menu_actions[ch]()
            except KeyError:
                print("Invalid selection, please try again.\n")
                menu_actions['main_menu']()
        return

    # Main menu
    @staticmethod
    def main_menu():
        Menu.clear()

        print("Welcome,\n")
        print("Please choose the menu you want to start:")
        print("1. Stats")
        print("2. Game")
        print("\n0. Quit")
        choice = input(" >>  ")
        if choice == '1':
            choice = 'stats'
        if choice == '2':
            choice = 'game'
        if choice == '0':
            sys.exit(404)
        Menu.exec_menu(choice)

    # Stat menu
    @staticmethod
    def stats():
        Menu.clear()
        print("Show stats,\n")
        print("Not available yet.\n")
        print("Actions:")
        print("0. Go back")
        choice = input(" >>  ")
        if choice == '0':
            choice = 'main_menu'
        Menu.exec_menu(choice)

    @staticmethod
    def game():
        Menu.clear()
        print("Game!,\n")
        print("Actions:")
        print("1. All random")
        print("2. Select chapter")
        print("0. Go back")
        choice = input(" >>  ")
        if choice == '1':
            choice = 'all_random'
        elif choice == '2':
            choice = 'select_chapter'
        elif choice == '0':
            choice = 'main_menu'

        Menu.exec_menu(choice)

    @staticmethod
    def game_all_random():
        g = Game(GameType.FULL_RANDOM, DataLoader.questions)
        Menu.clear()
        print("Game!,\n")
        choice = ''
        question_asked = 0
        while choice != '0':
            if env.SHOW_RESULTS and question_asked > 1 and question_asked % env.SHOW_RESULTS_FREQ == 0:
                Menu.clear()
                print(f'Congrats! You answered {g.correct_answers} questions correctly out of {g.seen} '
                      f'({g.correct_answers/g.seen} ratio)')
                input('>> (enter to continue)')

            Menu.clear()
            question = next(g)
            if env.PRINT_HEADER_QUESTION:
                print(f'{question.topic} - {question.question_number}')
            print(question.question)
            answers = list(question.answers)
            random.shuffle(answers)
            for i, answer in enumerate(answers):
                if env.PRINT_ANSWERS_LETTER:
                    print(f'{i + 1}. {answer}')
                else:
                    print(f'{i+1}. {" ".join(answer.split()[1:])}')

            choice = input('>> ')
            if g.answer_question(question, int(choice)-1):
                print('Correct answer!')
            else:
                print(f'Incorrect. Correct answer was: {question.correct_answer}: {question.answers[question.correct_answer]}')

            choice = input('>> (0 to exit, enter to continue)')
            question_asked += 1

        print(f'Saving results...')
        DataLoader.save_data_as_json(env.QUESTIONS_LOCATION)
        print(f'Save finish! Going to main menu.')
        Menu.exec_menu('main_menu')

    @staticmethod
    def game_select_chapter():
        print('Ha2')


menu_actions = {
    'main_menu': Menu.main_menu,
    'stats': Menu.stats,
    'game': Menu.game,
    'all_random': Menu.game_all_random,
    'select_chapter': Menu.game_select_chapter,
}