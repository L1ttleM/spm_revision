import re
import json
from code.question import Question
from os.path import exists


class DataLoader:
    questions = []

    @staticmethod
    def load_data_from_txt(filepath: str, topic: str):
        with open(filepath, 'r', encoding='utf-8') as f:
            data = f.read()

        data = data.split('No. Question and Answer')[1] if 'No. Question and Answer' in data else data
        chunks = re.split(r'^[0-9]+\. ', data, flags=re.M)[1:]
        load_questions = []

        print(f'Loading {len(chunks)} questions.')

        for i, chunk in enumerate(chunks):  # [0:1] [23:24] [12:13]
            print(i, chunk)
            # print(chunk)
            answers = []
            question = re.split(r'\nA\.?[\s\S]*\nB[^\n]', chunk)[0].replace('\n', ' ').strip()
            answers.append(re.findall(r'\nA\.?[\s\S]*\nB[^\n]', chunk)[0].replace('\n', ' ').strip()[0:-2])
            answers.append(re.findall(r'\nB\.?[\s\S]*\nC[^\n]', chunk)[0].replace('\n', ' ').strip()[0:-2])
            answers.append(re.findall(r'\nC\.?[\s\S]*\nD[^\n]', chunk)[0].replace('\n', ' ').strip()[0:-2])
            answers.append(re.findall(r'\nD\.?[\s\S]*\n', chunk)[0].replace('\n', ' ').strip()[0:-2])
            # print(i, re.findall(r'\nD\.?[\s\S]*\n[ABCD].?\n', chunk)[0].split('\n')[-2][0])
            correct_answer = re.findall(r'\nD\.?[\s\S]*\n[ABCD].?\n', chunk)[0].split('\n')[-2][0]
            # print(i, correct_answer, correct_answer[-1])
            correct_answer = ['A', 'B', 'C', 'D'].index(correct_answer[-1])

            new_question = Question(question, answers, topic, i+1, correct_answer)
            load_questions.append(new_question)

        print(f'Imported {len(load_questions)} questions.')
        DataLoader.questions += load_questions

    @staticmethod
    def load_data_from_json(filepath: str):
        if not exists(filepath):
            fp = open(filepath, "w")
            json.dump({}, fp, indent=4)
            fp.close()

        f = open(filepath, )
        json_questions = json.load(f)
        load_questions = []
        available_topics = set()

        for json_question in json_questions:
            new_question = Question.from_json(json_question)
            load_questions.append(new_question)
            available_topics.add(new_question.topic)

        DataLoader.questions = load_questions

        return available_topics

    @staticmethod
    def save_data_as_json(filepath: str):
        out_file = open(filepath, "w")
        json_questions = []

        for question in DataLoader.questions:
            json_questions.append(question.to_json())

        json.dump(json_questions, out_file, indent=4)


