from enum import Enum
from code.question import Question
import random


class GameType(Enum):
    FULL_RANDOM = 'full_random'
    WORST_10 = 'worst_10'


class Game:
    def __init__(self, game_type: GameType, questions: list[Question]):
        self.game_type = game_type
        self.questions = questions
        self.correct_answers = 0
        self.seen = 0
        self.possible_indexes = list(range(len(questions)))

    def __iter__(self):
        pass

    def has_more_question(self):
        return len(self.possible_indexes) > 0

    def __next__(self) -> Question:
        if len(self.possible_indexes) == 0:
            raise Exception("No more questions")
        index_question = random.randrange(0, len(self.possible_indexes))
        question = self.questions[self.possible_indexes[index_question]]
        del self.possible_indexes[index_question]
        question.seen += 1
        self.seen += 1
        return question

    def answer_question(self, question: Question, answer: str):
        if question.answers[question.correct_answer] == answer:
            question.correct += 1
            self.correct_answers += 1
            return True
        return False
