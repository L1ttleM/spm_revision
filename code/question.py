class Question:
    def __init__(self, question, answers, topic, question_number, correct_answer, seen=0, correct=0):
        self.question = question
        self.answers = answers
        self.topic = topic
        self.question_number = question_number
        self.correct_answer = correct_answer  # index of the right answer
        self.seen = seen
        self.correct = correct
        self.ratio = 0

        if self.seen != 0:
            self.ratio = self.correct / self.seen

    def __repr__(self):
        res = f'Question| {self.question}'
        for i, answer in enumerate(self.answers):
            res += f'\n{i}| {answer}'
        return res

    def to_json(self):
        return {
            'question': self.question,
            'answers': self.answers,
            'topic': self.topic,
            'question_number': self.question_number,
            'correct_answer': self.correct_answer,
            'seen': self.seen,
            'correct': self.correct,
        }

    @staticmethod
    def from_json(json: dict) -> 'Question':
        return Question(
            json['question'],
            json['answers'],
            json['topic'],
            json['question_number'],
            json['correct_answer'],
            json['seen'],
            json['correct']
        )
