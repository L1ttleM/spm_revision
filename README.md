# Revise SPM!
A small tool to revise SPM. Unfinished product with bugs, but enough works to tryhard :D

## Initialisation
- download/clone the project
- create a folder at the source named "data"
- copy the json questions into this data folder

No need to create a python virtual environment since it does not use other libraries.

## Run the program
- `python main.py -i file1 file2...` to import txt files in the json bank of questions
- `python main.py -g` to launch a game with all questions
- `python main.py -g -t 01 02 03` to launch a game with topics 01 02 03

To save, answer a question and then enter "0" and press enter. This will save the questions you have seen and if you correctly answered it.
