import argparse
import sys
import env
import os
import random

from code.dataloader import DataLoader
from os.path import exists
from code.game import Game, GameType

parser = argparse.ArgumentParser(description='Prepare your SPM exam!')
parser.add_argument('-i', '--import-files', nargs='+', default=[],
                    help='import the files to the bank of questions')
parser.add_argument('-s', '--stats', action='store_true', help='show the stats')
parser.add_argument('-g', '--game', action='store_true', help='launch a game')
parser.add_argument('-t', '--topics', nargs='+', default=[], help='specify topics for the game')

args = parser.parse_args()


def clear():
    # for windows
    if os.name == 'nt':
        _ = os.system('cls')
    # for mac and linux(here, os.name is 'posix')
    else:
        _ = os.system('clear')


def show_results(g: Game):
    clear()
    print(f'Congrats! You answered {g.correct_answers} questions correctly out of {g.seen} '
          f'({g.correct_answers / g.seen} ratio)')
    input('>> (enter to continue)')


def play_game(topics):
    if len(topics) > 0:
        questions_list = [q for q in DataLoader.questions if q.topic in topics]
    else:
        questions_list = DataLoader.questions

    g = Game(GameType.FULL_RANDOM, questions_list)
    clear()
    print("Game!,\n")
    choice = ''
    question_asked = 0

    while choice != '0' and g.has_more_question():
        if env.SHOW_RESULTS and question_asked > 1 and question_asked % env.SHOW_RESULTS_FREQ == 0:
            show_results(g)

        clear()
        question = next(g)
        print(f'Question {question_asked+1}/{len(questions_list)}')
        if env.PRINT_HEADER_QUESTION:
            print(f'Topic: {question.topic} - question number: {question.question_number}')
        print(question.question)
        answers = list(question.answers)
        random.shuffle(answers)
        for i, answer in enumerate(answers):
            if env.PRINT_ANSWERS_LETTER:
                print(f'{i + 1}. {answer}')
            else:
                print(f'{i + 1}. {" ".join(answer.split()[1:])}')

        choice = input('>> ')
        while choice not in ["1", "2", "3", "4"]:
            choice = input('>> ')
        if g.answer_question(question, answers[int(choice) - 1]):
            print('Correct answer!')
        else:
            print(
                f'Incorrect. Correct answer was: {question.answers[question.correct_answer]}')

        choice = input('>> (0 to exit, enter to continue)')
        while choice not in ["0", ""]:
            choice = input('>> (0 to exit, enter to continue)')
        question_asked += 1

    print(f'Saving results...')
    DataLoader.save_data_as_json(env.QUESTIONS_LOCATION)
    print(f'Save finish! Going to main menu.')
    if choice == '0':
        sys.exit()
    else:
        show_results(g)
        play_game(topics)


# If there is an argument, then we import the file and don't play the game.
if args.import_files:
    import_files = args.import_files
    for import_file in import_files:
        file_exists = exists(env.DATA_LOCATION+import_file)
        if not file_exists:
            print(f'Error, the file "{env.DATA_LOCATION+import_file}" does not exist')
            sys.exit(404)

    for import_file in import_files:
        # First load the already known question to add the new one to it
        DataLoader.load_data_from_json(env.QUESTIONS_LOCATION)

        filename = import_file.split('/')[-1] if '/' in import_file else import_file
        filename = filename.split('.')[-2] if '.' in filename else filename

        DataLoader.load_data_from_txt(env.DATA_LOCATION+import_file, filename)
        DataLoader.save_data_as_json(env.QUESTIONS_LOCATION)


# Show stats
elif args.stats:
    pass

# Play game
elif args.game:
    available_topics = DataLoader.load_data_from_json(env.QUESTIONS_LOCATION)
    topics = args.topics
    if topics:
        for topic in topics:
            if topic not in available_topics:
                print(f'Error, topic {topic} does not exist.')
                sys.exit(404)
    play_game(topics)
